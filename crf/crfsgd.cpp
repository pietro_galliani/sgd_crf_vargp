// -*- C++ -*-
// CRF with stochastic gradient
// Copyright (C) 2007- Leon Bottou

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA



// $Id$


#include "wrapper.h"
#include "vectors.h"
#include "matrices.h"
#include "gzstream.h"
#include "pstream.h"
#include "timer.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <algorithm>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <random>


using namespace std;

#if __cplusplus >= 201103L
# define HAS_UNORDEREDMAP
#elif defined(_LIBCPP_VERSION) && _LIBCPP_VERSION >=1100
# define HAS_UNORDEREDMAP
#elif defined(_MSC_VER) && _MSC_VER >= 1600
# define HAS_UNORDEREDMAP
#elif defined(__GXX_EXPERIMENTAL_CXX0X__)
# define HAS_UNORDEREDMAP
#endif

#ifdef HAS_UNORDEREDMAP
# include <unordered_map>
# define hash_map unordered_map
#elif defined(__GNUC__)
# define _GLIBCXX_PERMIT_BACKWARD_HASH
# include <ext/hash_map>
using __gnu_cxx::hash_map;
namespace __gnu_cxx {
  template<>
  struct hash<string> {
    hash<char*> h;
    inline size_t operator()(const string &s) const { return h(s.c_str()); };
  };
};
#else
# define hash_map map
#endif

#ifndef HUGE_VAL
# define HUGE_VAL 1e+100
#endif

typedef vector<string> strings_t;
typedef vector<int> ints_t;

bool verbose = true;
bool vargpdata = false;
bool use_fb = false;
string save_folder = "";

            
mt19937 gen;


// ============================================================
// Utilities


static int
skipBlank(istream &f)
{
  int c = f.get();
  while (f.good() && isspace(c) && c!='\n' && c!='\r')
    c = f.get();
  if (! f.eof())
    f.unget();
  return c;
}


static int
skipSpace(istream &f)
{
  int c = f.get();
  while (f.good() && isspace(c))
    c = f.get();
  if (! f.eof())
    f.unget();
  return c;
}


inline double
expmx(double x)
{
#define EXACT_EXPONENTIAL 0
#define IEEE754_EXPONENTIAL 0
#if EXACT_EXPONENTIAL
  return exp(-x);
#elif IEEE754_EXPONENTIAL
  // fast approximation using IEEE754 bit patterns.
  float p = x * -1.442695040f;
  float clipp = (p < -126.0f) ? -126.0f : p;
  union { unsigned int i; float f; } v;
  v.i = (unsigned int)( (1 << 23) * (clipp + 126.94269504f) );
  return v.f;
#else
  // fast approximation of exp(-x) for x positive
# define A0   (1.0)
# define A1   (0.125)
# define A2   (0.0078125)
# define A3   (0.00032552083)
# define A4   (1.0172526e-5)
  if (x < 13.0)
    {
      assert(x>=0);
      double y;
      y = A0+x*(A1+x*(A2+x*(A3+x*A4)));
      y *= y;
      y *= y;
      y *= y;
      y = 1/y;
      return y;
    }
  return 0;
# undef A0
# undef A1
# undef A2
# undef A3
# undef A4
#endif
}


static double
logSum(const VFloat *v, int n)
{
  int i;
  VFloat m = v[0];
  for (i=0; i<n; i++)
    m = max(m, v[i]);
  double s = 0;
  for (i=0; i<n; i++)
    s += expmx(m-v[i]);
  return m + log(s);
}


static double
logSum(const FVector &v)
{
  return logSum(v, v.size());
}


static void
dLogSum(double g, const VFloat *v, VFloat *r, int n)
{
  int i;
  VFloat m = v[0];
  for (i=0; i<n; i++)
    m = max(m, v[i]);
  double z = 0;
  for (i=0; i<n; i++)
    {
      double e = expmx(m-v[i]);
      r[i] = e;
      z += e;
    }
  for (i=0; i<n; i++)
    r[i] = g * r[i] / z;
}


static void
dLogSum(double g, const FVector &v, FVector &r)
{
  assert(v.size() <= r.size());
  dLogSum(g, v, r, v.size());
}


class ixstream_t
{
  bool z;
  ifstream fn;
  igzstream fz;

public:
  ixstream_t(const char *name)
  {
    string fname = name;
    int len = fname.size();
    z = fname.substr(len-3) == ".gz";
    if (z)
      fz.open(name);
    else
      fn.open(name);
  }
  istream& stream()
  {
    if (z)
      return fz;
    else
      return fn;
  }
};


// ============================================================
// Parsing data file


int
readDataLine(istream &f, strings_t &line, int &expected)
{
  int obtained = 0;
  while (f.good())
    {
      int c = skipBlank(f);
      if (c == '\n' || c == '\r')
        break;
      string s;
      f >> s;
      if (! s.empty())
        {
          line.push_back(s);
          obtained += 1;
        }
    }
  int c = f.get();
  if (c == '\r' && f.get() != '\n')
    f.unget();
  if (obtained > 0)
    {
      if (expected <= 0)
        expected = obtained;
      else if (expected > 0 && expected != obtained)
        {
          cerr << "ERROR: expecting " << expected
               << " columns in data file." << endl;
          exit(10);
        }
    }
  else
    skipSpace(f);
  return obtained;
}


int
readDataSentence(istream &f, strings_t &s, int &expected)
{
  strings_t line;
  s.clear();
  while (f.good())
    if (readDataLine(f, s, expected))
      break;
  while (f.good())
    if (! readDataLine(f, s, expected))
      break;
  if (expected)
    return s.size() / expected;
  return 0;
}



// ============================================================
// Processing templates


void
checkTemplate(string tpl)
{
  const char *p = tpl.c_str();
  if (p[0]!='U' && p[0]!='B')
    {
      cerr << "ERROR: Unrecognized template type (neither U nor B.)" << endl
           << "       Template was \"" << tpl << "\"." << endl;
      exit(10);
    }
  while (p[0])
    {
      if (p[0]=='%' && p[1]=='x')
        {
          bool okay = false;
          char *n = const_cast<char*>(p);
          long junk; (void)junk; // not used
          if (n[2]=='[') {
            junk = strtol(n+3,&n, 10);
            while (isspace(n[0]))
              n += 1;
            if (n[0] == ',') {
              junk = strtol(n+1, &n, 10);
              while (isspace(n[0]))
                n += 1;
              if (n[0] == ']')
                okay = true;
            }
          }
          if (okay)
            p = n;
          else {
            cerr << "ERROR: Syntax error in %x[.,,] expression." << endl
                 << "       Template was \"" << tpl << "\"." << endl;
            exit(10);
          }
        }
      p += 1;
    }
}


string
expandTemplate(string tpl, const strings_t &s, int columns, int pos)
{
  string e;
  int rows = s.size() / columns;
  const char *t = tpl.c_str();
  const char *p = t;

  static const char *BOS[4] = { "_B-1", "_B-2", "_B-3", "_B-4"};
  static const char *EOS[4] = { "_B+1", "_B+2", "_B+3", "_B+4"};

  while (*p)
    {
      if (p[0]=='%' && p[1]=='x' && p[2]=='[')
        {
          if (p > t)
            e.append(t, p-t);
          // parse %x[A,B] assuming syntax has been verified
          char *n;
          int a = strtol(p+3, &n, 10);
          while (n[0] && n[0]!=',')
            n += 1;
          int b = strtol(n+1, &n, 10);
          while (n[0] && n[0]!=']')
            n += 1;
          p = n;
          t = n+1;
          // catenate
          a += pos;
          if (b>=0 && b<columns)
            {
              if (a>=0 && a<rows)
                e.append(s[a*columns+b]);
              else if (a<0)
                e.append(BOS[min(3,-a-1)]);
              else if (a>=rows)
                e.append(EOS[min(3,a-rows)]);
            }
        }
      p += 1;
    }
  if (p > t)
    e.append(t, p-t);
  return e;
}


void
readTemplateFile(const char *fname, strings_t &templateVector)
{
  ifstream f(fname);
  if (! f.good())
    {
      cerr << "ERROR: Cannot open " << fname << " for reading." << endl;
      exit(10);
    }
  while(f.good())
    {
      int c = skipSpace(f);
      while (c == '#')
        {
          while (f.good() && c!='\n' && c!='\r')
            c = f.get();
          f.unget();
          c = skipSpace(f);
        }
      string s;
      getline(f,s);
      if (! s.empty())
        {
          checkTemplate(s);
          templateVector.push_back(s);
        }
    }
  if (! f.eof())
    {
      cerr << "ERROR: Cannot read " << fname << " for reading." << endl;
      exit(10);
    }
}



// ============================================================
// Dictionary


typedef hash_map<string,int> dict_t;

class Dictionary
{
private:
  dict_t outputs;
  dict_t features;
  strings_t templates;
  strings_t outputnames;
  mutable dict_t internedStrings;
  int index;

public:
  Dictionary() : index(0) { }

  int nOutputs() const { return outputs.size(); }
  int nFeatures() const { return features.size(); }
  int nTemplates() const { return templates.size(); }
  int nParams() const { return index; }

  int output(string s) const {
    dict_t::const_iterator it = outputs.find(s);
    return (it != outputs.end()) ? it->second : -1;
  }

  int feature(string s) const {
    dict_t::const_iterator it = features.find(s);
    return (it != features.end()) ? it->second : -1;
  }

  string outputString(int i) const { return outputnames.at(i); }
  string templateString(int i) const { return templates.at(i); }


  string internString(string s) const;

  int initFromData(const char *tFile, const char *dFile, int cutoff=1);
  void initFromFolder(string dataFolder, ints_t indexes, int cutoff);

  friend istream& operator>> ( istream &f, Dictionary &d );
  friend ostream& operator<< ( ostream &f, const Dictionary &d );
};



string
Dictionary::internString(string s) const
{
  dict_t::const_iterator it = internedStrings.find(s);
  if (it != internedStrings.end())
    return it->first;
#if defined(mutable)
  const_cast<Dictionary*>(this)->
#endif
  internedStrings[s] = 1;
  return s;
}


ostream&
operator<<(ostream &f, const Dictionary &d)
{
  typedef map<int,string> rev_t;
  rev_t rev;
  strings_t::const_iterator si;
  dict_t::const_iterator di;
  rev_t::const_iterator ri;
  for (di=d.outputs.begin(); di!=d.outputs.end(); di++)
    rev[di->second] = di->first;
  for (ri=rev.begin(); ri!=rev.end(); ri++)
    f << "Y" << ri->second << endl;
  for (si=d.templates.begin(); si!=d.templates.end(); si++)
    f << "T" << *si << endl;
  rev.clear();
  for (di=d.features.begin(); di!=d.features.end(); di++)
    rev[di->second] = di->first;
  for (ri=rev.begin(); ri!=rev.end(); ri++)
    f << "X" << ri->second << endl;
  return f;
}


istream&
operator>>(istream &f, Dictionary &d)
{
  d.outputs.clear();
  d.features.clear();
  d.templates.clear();
  d.index = 0;
  int findex = 0;
  int oindex = 0;
  while (f.good())
    {
      string v;
      skipSpace(f);
      int c = f.get();
      if  (c == 'Y')
        {
          f >> v;
          if (v.empty())
            {
              cerr << "ERROR (reading dictionary): "
                   << "Invalid Y record in model file." << endl;
              exit(10);
            }
          if (findex>0)
            {
              cerr << "ERROR (reading dictionary): "
                   << "Found Y record occuring after X record." << endl;
              exit(10);
            }
          d.outputs[v] = oindex++;
        }
      else if (c == 'T')
        {
          f >> v;
          if (v.empty())
            {
              cerr << "ERROR (reading dictionary): "
                   << "Invalid T record." << endl;
              exit(10);
            }
          checkTemplate(v);
          d.templates.push_back(v);
        }
      else if (c == 'X')
        {
          f >> v;
          if (v.empty())
            {
              cerr << "ERROR (reading dictionary): "
                   << "Invalid X record." << endl;
              exit(10);
            }
          int nindex = findex;
          if (v[0]=='U')
            nindex += oindex;
          else if (v[0]=='B')
            nindex += oindex * oindex;
          else
            {
              cerr << "ERROR (reading dictionary): "
                   << "Invalid feature in X record: " << v << endl;
              exit(10);
            }
          d.features[v] = findex;
          findex = nindex;
        }
      else
        {
          f.unget();
          break;
        }
    }
  d.index = findex;
  if (!f.good() && !f.eof())
    {
      d.outputs.clear();
      d.features.clear();
      d.templates.clear();
      d.index = 0;
    }
  d.outputnames.resize(oindex);
  for (dict_t::const_iterator it=d.outputs.begin();
       it!=d.outputs.end(); it++)
    d.outputnames[it->second] = it->first;
  return f;
}



typedef pair<string,int> sipair_t;
typedef vector<sipair_t> sivector_t;

struct SIPairCompare {
  bool operator() (const sipair_t &p1, const sipair_t &p2) {
    if (p1.second > p2.second) return true;
    else if (p1.second < p2.second) return false;
    else return (p1.first < p2.first);
  }
} siPairCompare;

void Dictionary::initFromFolder(string dataFolder, ints_t indexes, int cutoff){
  // clear all
  templates.clear();
  outputs.clear();
  features.clear();
  index = 0;


  string info_folder = dataFolder + "/info.txt";

  if (verbose)
    cout << "Reading task information from " << info_folder << endl;

  ifstream fo;
  fo.open(info_folder.c_str());

  int nfeatures = -1;
  int nlabels = -1;
  int myval;


  while (fo.is_open()) {
    string desc;

    fo >> myval >> desc;

    if (fo.eof())
        break;

    if (fo.eof())
        break;

    cout << "READ DESCRIPTION " << desc << endl;

    if (desc == "FEATURES")
      nfeatures = myval;

    if (desc == "LABELS")
      nlabels = myval;
  }
  fo.close();
  if (nfeatures <= 0 || nlabels <= 0) {
    cerr << "ERROR: contents of file " << info_folder << " not correct" << endl;
    exit(10);
  }

  cout << nfeatures<< " features, " << nlabels << " labels." << endl;

  index = nlabels * nlabels;
  features["B"] = 0;

  for (int i = 0; i < nfeatures; i++) {
    string feature_i = "U"+std::to_string(i);
    feature_i = internString(feature_i);
    features[feature_i] = index;
    index += nlabels;
  }


  outputnames.resize(nlabels);
  for (int i = 0; i < nlabels; i++) {
    string label_i = std::to_string(i);
    label_i = internString("L"+std::to_string(i));
    outputs[label_i] = i;
    outputnames[i] = label_i;
    }



}

int
Dictionary::initFromData(const char *tFile, const char *dFile, int cutoff)
{

  // clear all
  templates.clear();
  outputs.clear();
  features.clear();
  index = 0;


  // read templates
  if (verbose)
    cout << "Reading template file " << tFile << "." << endl;
  readTemplateFile(tFile, templates);
  int nu = 0;
  int nb = 0;
  for (unsigned int t=0; t<templates.size(); t++)
    if (templates[t][0]=='U')
      nu += 1;
    else if (templates[t][0]=='B')
      nb += 1;
  if (verbose)
    cout << "  u-templates: " << nu
         << "  b-templates: " << nb << endl;
  if (nu + nb != (int)templates.size())
    {
      cerr << "ERROR (building dictionary): "
           << "Problem counting templates" << endl;
      exit(10);
    }

  // process compressed datafile
  if (verbose)
    cerr << "Scanning " << dFile << " to build dictionary." << endl;
  typedef hash_map<string,int> hash_t;
  hash_t fcount;
  int columns = 0;
  int oindex = 0;
  int sentences = 0;
  strings_t s;
  ixstream_t fx(dFile);
  istream &f = fx.stream();
  Timer timer;
  timer.start();
  while (readDataSentence(f, s, columns))
    {
      sentences += 1;
      // intern strings to save memory
      for (strings_t::iterator it=s.begin(); it!=s.end(); it++)
        *it = internString(*it);
      // expand features and count them
      int rows = s.size()/columns;
      for (int pos=0; pos<rows; pos++)
        {
          // check output keyword
          string &y = s[pos*columns+columns-1];
          dict_t::iterator di = outputs.find(y);
          if (di == outputs.end())
            outputs[y] = oindex++;
          // expand templates
          for (unsigned int t=0; t<templates.size(); t++)
            {
              string x = expandTemplate(templates[t], s, columns, pos);
              hash_t::iterator hi = fcount.find(x);
              if (hi != fcount.end())
                hi->second += 1;
              else
                fcount[x] = 1;
            }
        }
    }
  if (! f.eof())
    {
      cerr << "ERROR (building dictionary): "
           << "Problem reading data file " << dFile << endl;
      exit(10);
    }
  outputnames.resize(oindex);
  for (dict_t::const_iterator it=outputs.begin(); it!=outputs.end(); it++)
    outputnames[it->second] = it->first;
  if (verbose)
    cout << "  sentences: " << sentences
         << "  outputs: " << oindex << endl;

  // sorting in frequency order
  sivector_t keys;
  for (hash_t::iterator hi = fcount.begin(); hi != fcount.end(); hi++)
    if (hi->second >= cutoff)
      keys.push_back(*hi);
  if (keys.size() <= 0)
    {
      cerr << "ERROR (building dictionary): "
           << "No features satisfy the cutoff frequency" << endl;
      exit(10);
    }
  sort(keys.begin(), keys.end(), siPairCompare);

  // allocating parameters
  for (unsigned int j=0; j<keys.size(); j++)
    {
      string k = keys[j].first;
      features[k] = index;
      if (k[0] == 'B')
        index += oindex * oindex;
      else
        index += oindex;
    }

  /*string mytest = "NN";

  dict_t::const_iterator it = internedStrings.find(mytest);
  if (it != internedStrings.end()) {
    cout << "Found!" << endl;
  }*/

  if (verbose)
    cout << "  cutoff: " << cutoff
         << "  features: " << features.size()
         << "  parameters: " << index << endl
         << "  duration: " << timer.elapsed() << " seconds." << endl;

  /*dict_t::const_iterator it = features.find("U22:VBZ/NNS/.");
  cout << "first: " << it->first << ", second: " << it->second << endl;*/
  return sentences;
}



// ============================================================
// Preprocessing data


typedef vector<SVector> svec_t;
typedef vector<int> ivec_t;


class Sentence
{
private:
  struct Rep
  {
    int refcount;
    int columns;
    strings_t data;
    svec_t uFeatures;
    svec_t bFeatures;
    ivec_t yLabels;
    Rep *copy() { return new Rep(*this); }
  };
  Wrapper<Rep> w;
  Rep *rep() { return w.rep(); }
  const Rep *rep() const { return w.rep(); }

public:
  Sentence() {}

  void init(const Dictionary &dict, const strings_t &s, int columns);
  void init_vargp(const Dictionary &dict, string filename);

  int size() const { return rep()->uFeatures.size(); }
  SVector u(int i) const { return rep()->uFeatures.at(i); }
  SVector b(int i) const { return rep()->bFeatures.at(i); }
  int y(int i) const { return rep()->yLabels.at(i); }
  ints_t yLabels() {return rep()->yLabels;}

  int columns() const { return rep()->columns; }
  string data(int pos, int col) const;

  friend ostream& operator<<(ostream &f, const Sentence &s);
  
  void save_vargp(string folder, string name);
};

void Sentence::save_vargp(string folder, string name) { 
    cout << " saving " << name << " in folder " << folder <<endl; 
    cout.flush();
    Rep *r = rep();
    ofstream fo((folder + "/" + name + ".x").c_str());
    int i =1;
    //int q = 0; 
    //cout << " name = " << name << ", size = " << r->uFeatures.size() << endl; 
    //cin >> q;
    for (SVector v : r->uFeatures) { 
        v.savetxt(fo, i); 
        i++;
    }
    //cout << "q = " << q << endl;
    fo.close(); 
    fo.open((folder + "/" + name + ".y").c_str());
    for (int y : yLabels()) { 
        fo << y << endl;
    }
    fo.close();
        
}

void Sentence::init_vargp(const Dictionary &dict, string filename){
  w.detach();
  Rep *r = rep();
  //int ntemplat = dict.nTemplates();

  r->uFeatures.clear();
  r->bFeatures.clear();
  r->yLabels.clear();
  r->columns = dict.nFeatures();

  ifstream fo;
  fo.open(filename + ".x");

  unsigned int sentence_length = 0;

  unsigned int pos, feat, val;


  while (fo >> pos >> feat >> val){

    if (pos > r->uFeatures.size()){
        r->uFeatures.resize(pos);
        SVector u;
        r->uFeatures[pos-1] = u;
        sentence_length = pos;
        }

    string feat_s = "U" + to_string(feat);
    int findex = dict.feature(feat_s);
    if (findex >= 0)
    {
            r->uFeatures[pos-1].set(findex, val);
    }

  }

  fo.close();

  for (unsigned int i = 0; i < sentence_length - 1; i++){
      SVector b;
      b.set(dict.feature("B"), 1);
      r->bFeatures.push_back(b);
  }

  unsigned int num_outputs = 0;
  int next_output;
  fo.open(filename + ".y");
  while (fo >> next_output) {
    num_outputs++;
    if (next_output < 0) 
        next_output = 0; 

    r->yLabels.push_back(dict.output("L"+to_string(next_output)));
  }

  fo.close();

  if (num_outputs != sentence_length) {
    cout << "ERROR: numbers of words in " << filename << ".x  and " << filename << ".y differ." << endl;
    exit(10);
  }
}



void
Sentence::init(const Dictionary &dict, const strings_t &s, int columns)
{
  w.detach();
  Rep *r = rep();
  int maxcol = columns - 1;
  int maxpos = s.size()/columns - 1;
  int ntemplat = dict.nTemplates();
  r->uFeatures.clear();
  r->bFeatures.clear();
  r->yLabels.clear();
  r->columns = columns;
  // intern strings to save memory
  for (strings_t::const_iterator it=s.begin(); it!=s.end(); it++)
    r->data.push_back(dict.internString(*it));
  // expand features
  for (int pos=0; pos<=maxpos; pos++)
    {
      // labels
      string y = s[pos*columns+maxcol];
      int yindex = dict.output(y);
      r->yLabels.push_back(yindex);
      // features
      SVector u;
      SVector b;
      for (int t=0; t<ntemplat; t++)
        {
          string tpl = dict.templateString(t);
          int findex = dict.feature(expandTemplate(tpl, s, columns, pos));
          if (findex >= 0)
            {
              if (tpl[0]=='U')
                u.set(findex, 1);
              else if (tpl[0]=='B')
                b.set(findex, 1);
            }
        }
      r->uFeatures.push_back(u);
      if (pos < maxpos) {
        r->bFeatures.push_back(b);
       }

    }
    /*
    for (unsigned int i = 0; i < r->bFeatures.size(); i++)
       cout << r->bFeatures[i].get(0);

    cout << endl;

    for (SVector b : r->bFeatures) {
        cout << b.get(0) << " ";
    }
    cout << endl;*/
}


string
Sentence::data(int pos, int col) const
{
  const Rep *r = rep();
  if (pos>=0 && pos<size())
    if (col>=0 && col<r->columns)
      return r->data[pos*r->columns+col];
  return string();
}


ostream&
operator<<(ostream &f, const Sentence &s)
{
  int maxpos = s.size() - 1;
  int columns = s.columns();
  for (int pos = 0; pos<=maxpos; pos++) {
    for (int col = 0; col<columns; col++)
      f << s.data(pos, col) << " ";
    f << endl << "   Y" << pos << " " << s.y(pos) << endl;
    f << "   U" << pos << s.u(pos);
    if (pos < maxpos)
      f << "   B" << pos << s.b(pos);
  }
  return f;
}


typedef vector<Sentence> dataset_t;


void
loadSentences(const char *fname, const Dictionary &dict, dataset_t &data, int offset)
{
  if (verbose)
    cout << "Reading and preprocessing " << fname << "." << endl;
  Timer timer;
  int sentences = 0;
  int columns = 0;
  strings_t s;
  ixstream_t fx(fname);
  istream &f = fx.stream();
  timer.start();
  while (readDataSentence(f, s, columns))
    {
      Sentence ps;
      ps.init(dict, s, columns);
      if (save_folder.length()>0)
        ps.save_vargp(save_folder, std::to_string(offset + sentences+1));

      data.push_back(ps);
      sentences += 1;
    }
  if (verbose)
    cout << "  processed: " << sentences << " sentences." << endl
         << "  duration: " << timer.elapsed() << " seconds." << endl;
}

void loadSentences_vargp(string dataFolder, const Dictionary &dict, ints_t indexes, unsigned int ntraining, dataset_t &train, dataset_t &test){
    if (verbose)
        cout << "Reading data from folder " << dataFolder << "." << endl;

    for (unsigned int i =0; i < indexes.size(); i++){
        Sentence ps;
        ps.init_vargp(dict, dataFolder + "/" + std::to_string(indexes[i]));
        if (i < ntraining)
          train.push_back(ps);
        else
          test.push_back(ps);

        if (i % 100 == 0)
          cout << "i = " << i << ", going up to " << indexes.size() << endl;
    }


}

ints_t loadIndexes(const char* filename)
{
  ints_t indexes;
  if (verbose)
    cout << "Reading index file " << filename << "." << endl;
    int sentences = 0;
    ifstream fo;
    fo.open(filename);
    while (fo.is_open()) {
      double n;
      fo >> n;
      if (fo.eof())
        break;

      indexes.push_back((int)n);
      sentences ++;
    }
    fo.close();
    if (verbose) {}
        cout << "Processed: " << sentences << " indexes." << endl;

    return indexes;
}




// ============================================================
// Scorer


class Scorer
{
public:
  Sentence s;
  const Dictionary &d;
  VFloat *w;
  double &wscale;
  bool scoresOk;
  vector<FVector> uScores;
  vector<FMatrix> bScores;

  Scorer(const Sentence &s_, const Dictionary &d_, FVector &w_, double &c_);
  void computeScores();
  virtual ~Scorer() {}
  virtual void uGradients(const VFloat *g, int pos, int fy, int ny) {}
  virtual void bGradients(const VFloat *g, int pos, int fy, int ny, int y) {}

  double viterbi(ints_t &path);
  int test(double &nell);
  int test(ostream &f, double& nell);
  double scoreCorrect();
  double gradCorrect(double g);
  double scoreForward();
  double gradForward(double g);

  int findBest(FVector scores, int npos);
  double forwardBackward(ints_t &path, ivec_t yLabels);
};


Scorer::Scorer(const Sentence &s_, const Dictionary &d_, FVector &w_, double &c_)
  : s(s_), d(d_), w(w_), wscale(c_), scoresOk(false)
{
  assert(w_.size() == d.nParams());
}

void
Scorer::computeScores()
{
  if (! scoresOk)
    {
      int nout = d.nOutputs();
      int npos = s.size();
      int pos;
      // compute uScores
      uScores.resize(npos);
      for (pos=0; pos<npos; pos++)
        {
          FVector &u = uScores[pos];
          u.resize(nout);
          SVector x = s.u(pos);
          for (const SVector::Pair *p = x; p->i >= 0; p++)
            for (int j=0; j<nout; j++)
              u[j] += w[p->i + j] * p->v;
          for (int j=0; j<nout; j++)
            u[j] *= wscale;
        }
      // compute bScores
      bScores.resize(npos-1);
      for (pos=0; pos<npos-1; pos++)
        {
          FMatrix &b = bScores[pos];
          b.resize(nout,nout);
          SVector x = s.b(pos);
          for (const SVector::Pair *p = x; p->i >= 0; p++)
            {
              int k = 0;
              for (int i=0; i<nout; i++)
                {
                  FVector &bi = b[i];
                  for (int j=0; j<nout; j++, k++)
                    bi[j] += w[p->i + k] * p->v;
                }
            }
          for (int i=0; i<nout; i++)
            {
              FVector &bi = b[i];
              for (int j=0; j<nout; j++)
                bi[j] *= wscale;
            }
        }
    }
  scoresOk = true;
}


double
Scorer::viterbi(ints_t &path)
{
  computeScores();
  int npos = s.size();
  int nout = d.nOutputs();
  int pos, i, j;

  // allocate backpointer array
  vector<ints_t> pointers(npos);
  for (int i=0; i<npos; i++)
    pointers[i].resize(nout);

  // process scores
  FVector scores = uScores[0];
  for (pos=1; pos<npos; pos++)
    {
      FVector us = uScores[pos];
      for (i=0; i<nout; i++)
        {
          FVector bs = bScores[pos-1][i];
          bs.add(scores);
          int bestj = 0;
          double bests = bs[0];
          for (j=1; j<nout; j++)
            if (bs[j] > bests)
              { bests = bs[j]; bestj = j; }
          pointers[pos][i] = bestj;
          us[i] += bests;
        }
      scores = us;
    }
  // find best final score
  int bestj = 0;
  double bests = scores[0];
  for (j=1; j<nout; j++)
    if (scores[j] > bests)
      { bests = scores[j]; bestj = j; }
  // backtrack
  path.resize(npos);
  for (pos = npos-1; pos>=0; pos--)
    {
      path[pos] = bestj;
      bestj = pointers[pos][bestj];
    }
  return bests;
}

int Scorer::findBest(FVector scores, int npos){
  double best_score = scores[0];
  int best_idx = 0;

  /*for (int i = 0; i < npos; i++)
    cout << scores[i] << " ";
  cout << endl;*/

  for (int i = 1; i < npos; i++) {
    if (scores[i] > best_score) {
      best_score = scores[i];
      best_idx = i;
    }
  }
  return best_idx;
}

double get_value(FMatrix alpha, int i, int j){
    return alpha[i][j];
}

double
Scorer::forwardBackward(ints_t &path, ivec_t yLabels)
{
  computeScores();
  int npos = s.size();
  int nout = d.nOutputs();
  int pos;

  FMatrix alpha(npos,nout);
  FMatrix beta(npos,nout);
  FMatrix gamma(npos,nout);

  // forward
  alpha[0] = uScores[0];
  for (pos=1; pos<npos; pos++)
    {
      FVector us = uScores[pos];
      for (int i=0; i<nout; i++)
        {
          FVector bs = bScores[pos-1][i];
          bs.add(alpha[pos-1]);
          us[i] += logSum(bs);
        }
      alpha[pos] = us;
      //cout << logSum(alpha[pos]) << endl;
    }

  double p_fwd = logSum(alpha[npos-1]);

  // backward
  for (int i = 0; i < nout; i++)
    beta[pos-1][i] = 0;

  for (pos = pos-2; pos >= 0; pos--) {
    FVector us = uScores[pos+1];
    const FMatrix &bsc = bScores[pos];

    for (int i = 0; i < nout; i++) {
      FVector bs(nout);
      for (int j = 0; j < nout; j++)
        bs[j] = bsc[j][i] + us[j];
      bs.add(beta[pos+1]);

      beta[pos][i] = logSum(bs);
    }
  }

  /*
  beta[pos-1] = uScores[pos-1];
  for (pos=pos-2; pos>=0; pos--)
    {
      FVector us = uScores[pos];
      for (int i=0; i<nout; i++)
        {
          FVector bs(nout);
          const FMatrix &bsc = bScores[pos];
          for (int j=0; j<nout; j++)
            bs[j] = bsc[j][i];
          bs.add(beta[pos+1]);
          us[i] += logSum(bs);
        }
      beta[pos] = us;
    }*/

  // score
  //double score = logSum(beta[0]);



  // collect
  path.resize(npos);
  //gamma[0] = beta[0];
  //double cost = - gamma[0][yLabels[0]];
  //path[0] = findBest(gamma[0], nout);
  double cost = 0;
  //cout <<endl << "p_fwd = " << p_fwd << ", ";
  for (pos=0; pos<npos; pos++) {
    for (int j = 0; j < nout; j++)
      gamma[pos][j] = (alpha[pos][j] + beta[pos][j] - p_fwd);
    cost -= gamma[pos][yLabels[pos]];
    //cout << logSum(gamma[pos]) << " ";
    path[pos] = findBest(gamma[pos], nout);
  }
  //cout << endl;

  return cost;

}


int
Scorer::test(double &nell)
{
  ints_t path;
  int npos = s.size();
  int errors = 0;
  if (use_fb)
    nell += forwardBackward(path, s.yLabels());
  else
    viterbi(path);

  for (int pos=0; pos<npos; pos++)
    if (path[pos] != s.y(pos))
      errors += 1;
  return errors;
}

int
Scorer::test(ostream &f, double &nell)
{
  ints_t path;
  int npos = s.size();
  int ncol = s.columns();
  int errors = 0;

  if (use_fb)
    nell += forwardBackward(path, s.yLabels());
  else
    viterbi(path);

  for (int pos=0; pos<npos; pos++)
    {
      if (path[pos] != s.y(pos))
        errors += 1;
      for (int c=0; c<ncol; c++)
        f << s.data(pos,c) << " ";

      cout << "outputstring: " << d.outputString(path[pos]) << endl;
      f << d.outputString(path[pos]) << endl;
    }
  f << endl;
  return errors;
}


double
Scorer::scoreCorrect()
{
  computeScores();
  int npos = s.size();
  int y = s.y(0);
  double sum = uScores[0][y];
  for (int pos=1; pos<npos; pos++)
    {
      int fy = y;
      y = s.y(pos);
      if (y>=0 && fy>=0)
        sum += bScores[pos-1][y][fy];
      if (y>=0)
        sum += uScores[pos][y];
    }
  return sum;
}


double
Scorer::gradCorrect(double g)
{
  computeScores();
  int npos = s.size();
  int y = s.y(0);
  VFloat vf = g;
  uGradients(&vf, 0, y, 1);
  double sum = uScores[0][y];
  for (int pos=1; pos<npos; pos++)
    {
      int fy = y;
      y = s.y(pos);
      if (y>=0 && fy>=0)
        sum += bScores[pos-1][y][fy];
      if (y>=0)
        sum += uScores[pos][y];
      if (y>=0 && fy>=0)
        bGradients(&vf, pos-1, fy, 1, y);
      if (y>=0)
      uGradients(&vf, pos, y, 1);
    }
  return sum;
}


double
Scorer::scoreForward()
{
  computeScores();
  int npos = s.size();
  int nout = d.nOutputs();
  int pos, i;

  FVector scores = uScores[0];
  for (pos=1; pos<npos; pos++)
    {
      FVector us = uScores[pos];
      for (i=0; i<nout; i++)
        {
          FVector bs = bScores[pos-1][i];
          bs.add(scores);
          us[i] += logSum(bs);
        }
      scores = us;
    }
  return logSum(scores);
}



double
Scorer::gradForward(double g)
{
  computeScores();
  int npos = s.size();
  int nout = d.nOutputs();
  int pos;

#define USE_FORWARD_BACKWARD 0
#if USE_FORWARD_BACKWARD

  FMatrix alpha(npos,nout);
  FMatrix beta(npos,nout);
  // forward
  alpha[0] = uScores[0];
  for (pos=1; pos<npos; pos++)
    {
      FVector us = uScores[pos];
      for (int i=0; i<nout; i++)
        {
          FVector bs = bScores[pos-1][i];
          bs.add(alpha[pos-1]);
          us[i] += logSum(bs);
        }
      alpha[pos] = us;
    }

  // backward
  beta[pos-1] = uScores[pos-1];
  for (pos=pos-2; pos>=0; pos--)
    {
      FVector us = uScores[pos];
      for (int i=0; i<nout; i++)
        {
          FVector bs(nout);
          const FMatrix &bsc = bScores[pos];
          for (int j=0; j<nout; j++)
            bs[j] = bsc[j][i];
          bs.add(beta[pos+1]);
          us[i] += logSum(bs);
        }
      beta[pos] = us;
    }
  // score
  double score = logSum(beta[0]);

  // collect gradients
  for (pos=0; pos<npos; pos++)
    {
      FVector b = beta[pos];
      if (pos > 0)
        {
          FVector a = alpha[pos-1];
          const FMatrix &bsc = bScores[pos-1];
          for (int j=0; j<nout; j++)
            {
              FVector bs = bsc[j];
              FVector bgrad(nout);
              for (int i=0; i<nout; i++)
                bgrad[i] = g * expmx(max(0.0, score - bs[i] - a[i] - b[j]));
              bGradients(bgrad, pos-1, 0, nout, j);
            }
        }
      FVector a = alpha[pos];
      FVector us = uScores[pos];
      FVector ugrad(nout);
      for (int i=0; i<nout; i++)
        ugrad[i] = g * expmx(max(0.0, score + us[i] - a[i] - b[i]));
      uGradients(ugrad, pos, 0, nout);
    }

#else

  // forward
  FMatrix scores(npos, nout);
  scores[0] = uScores[0];
  for (pos=1; pos<npos; pos++)
    {
      FVector us = uScores[pos];
      for (int i=0; i<nout; i++)
        {
          FVector bs = bScores[pos-1][i];
          bs.add(scores[pos-1]);
          us[i] += logSum(bs);
        }
      scores[pos] = us;
    }
  double score = logSum(scores[npos-1]);

  // backward with chain rule
  FVector tmp(nout);
  FVector grads(nout);
  dLogSum(g, scores[npos-1], grads);
  for (pos=npos-1; pos>0; pos--)
    {
      FVector ug;
      uGradients(grads, pos, 0, nout);
      for (int i=0; i<nout; i++)
        if (grads[i])
          {
            FVector bs = bScores[pos-1][i];
            bs.add(scores[pos-1]);
            dLogSum(grads[i], bs, tmp);
            bGradients(tmp, pos-1, 0, nout, i);
            ug.add(tmp);
          }
      grads = ug;
    }
  uGradients(grads, 0, 0, nout);

#endif

  return score;
}



// ============================================================
// GScorer - compute gradients as SVectors


class GScorer : public Scorer
{
private:
  SVector grad;
public:
  GScorer(const Sentence &s_, const Dictionary &d_, FVector &w_, double &c_);
  void clear() { grad.clear(); }
  SVector gradient() { return grad; }
  virtual void uGradients(const VFloat *g, int pos, int fy, int ny);
  virtual void bGradients(const VFloat *g, int pos, int fy, int ny, int y);
};


GScorer::GScorer(const Sentence &s_,const Dictionary &d_,
                 FVector &w_, double &c_)
  : Scorer(s_, d_, w_, c_)
{
}


void
GScorer::uGradients(const VFloat *g, int pos, int fy, int ny)
{
  int n = d.nOutputs();
  assert(pos>=0 && pos<s.size());
  assert(fy>=0 && fy<n);
  assert(fy+ny>0 && fy+ny<=n);
  int off = fy;
  SVector x = s.u(pos);
  SVector a;
  for (const SVector::Pair *p = x; p->i>=0; p++)
    for (int j=0; j<ny; j++)
      a.set(p->i + off + j, g[j] * p->v);
  grad.add(a);
}


void
GScorer::bGradients(const VFloat *g, int pos, int fy, int ny, int y)
{
  int n = d.nOutputs();
  assert(pos>=0 && pos<s.size());
  assert(y>=0 && y<n);
  assert(fy>=0 && fy<n);
  assert(fy+ny>0 && fy+ny<=n);
  int off = y * n + fy;
  SVector x = s.b(pos);
  SVector a;
  for (const SVector::Pair *p = x; p->i>=0; p++)
    for (int j=0; j<ny; j++)
      a.set(p->i + off + j, g[j] * p->v);
  grad.add(a);
}



// ============================================================
// TScorer - training score: update weights directly


class TScorer : public Scorer
{
private:
  double eta;
public:
  TScorer(const Sentence &s_, const Dictionary &d_,
          FVector &w_, double &c_, double eta_);
  virtual void uGradients(const VFloat *g, int pos, int fy, int ny);
  virtual void bGradients(const VFloat *g, int pos, int fy, int ny, int y);
};


TScorer::TScorer(const Sentence &s_, const Dictionary &d_,
                 FVector &w_, double &c_, double eta_ )
  : Scorer(s_,d_,w_,c_), eta(eta_)
{
}


void
TScorer::uGradients(const VFloat *g, int pos, int fy, int ny)
{
  int n = d.nOutputs();
  assert(pos>=0 && pos<s.size());
  //cout << "fy: " << fy << ", n " << n << endl; 
  //cout.flush();
  assert(fy>=0 && fy<n);
  assert(fy+ny>0 && fy+ny<=n);
  int off = fy;
  SVector x = s.u(pos);
  double gain = eta / wscale;
  for (const SVector::Pair *p = x; p->i>=0; p++)
    for (int j=0; j<ny; j++)
      w[p->i + off + j] += g[j] * p->v * gain;
}


void
TScorer::bGradients(const VFloat *g, int pos, int fy, int ny, int y)
{
  int n = d.nOutputs();
  assert(pos>=0 && pos<s.size());
  assert(y>=0 && y<n);
  assert(fy>=0 && fy<n);
  assert(fy+ny>0 && fy+ny<=n);
  int off = y * n + fy;
  SVector x = s.b(pos);
  SVector a;
  double gain = eta / wscale;
  for (const SVector::Pair *p = x; p->i>=0; p++)
    for (int j=0; j<ny; j++)
      w[p->i + off + j] += g[j] * p->v * gain;
}




// ============================================================
// Main class CrfSgd




class CrfSgd
{
  Dictionary dict;
  double wscale;
  FVector w;
  double lambda;
  double wnorm;
  double t;
  int epoch;

  int num_training; 

  void load(istream &f);
  void save(ostream &f) const;
  void rescale();

  double findObjBySampling(const dataset_t &data, const ivec_t &sample);
  double tryEtaBySampling(const dataset_t &data, const ivec_t &sample,
                          double eta);

public:

  CrfSgd();

  int getEpoch() const { return epoch; }
  const Dictionary& getDict() const { return dict; }
  double getLambda() const { return lambda; }
  double getEta() const { return 1/(t*lambda); }
  FVector getW() const { const_cast<CrfSgd*>(this)->rescale(); return w; }


  void initialize(const char *templatefile,
                  const char *datafile,
                  double c = 4,
                  int cutoff = 3);


  // crf.initialize_vargp(dataFolder, indexes, c, cutoff);
  void initialize_vargp(string dataFolder, ints_t indexes, double c, int cutoff);

  void adjustEta(double seta=1);
  double adjustEta(const dataset_t &data,
		 int sample=500, double seta=1, Timer *tm=0);

  void train(const dataset_t &data, int epochs=1, Timer *tm=0);
  double test(const dataset_t &data, const char *conlleval=0, Timer *tm=0);
  void tag(const dataset_t &data);

  void crossValidate(const dataset_t &data, int num_folds, int min_exp, int max_exp, int epochs = 1, double eta=0, Timer *tm=0);
 

  friend istream& operator>> ( istream &f, CrfSgd &d );
  friend ostream& operator<< ( ostream &f, const CrfSgd &d );
};


CrfSgd::CrfSgd()
  : wscale(1), lambda(0), wnorm(0), t(0), epoch(0)
{
}

void
CrfSgd::load(istream &f)
{
  f >> dict;
  t = 0;
  epoch = 0;
  wscale = 0;
  w.clear();

  while (f.good())
    {
      skipSpace(f);
      int c = f.get();
      if (f.eof())
        break;
      if (c == 'T')
        {
          t = -1;
          f >> t;
          if (!f.good() || t <= 0)
            {
              cerr << "ERROR (reading model): "
                   << "Invalid iteration number: " << t << endl;
              exit(10);
            }
        }
      else if (c == 'E')
        {
          epoch = -1;
          f >> epoch;
          if (!f.good() || epoch < 0)
            {
              cerr << "ERROR (reading model): "
                   << "Invalid epoch number: " << epoch << endl;
              exit(10);
            }
        }
      else if (c == 'W')
        {
          w.clear();
          f >> w;
          if (! f.good() || w.size() != dict.nParams())
            {
              cerr << "ERROR (reading model): "
                   << "Invalid weight vector size: " << w.size() << endl;
              exit(10);
            }
          wnorm = dot(w,w);
          wscale = 1.0;
        }
      else if (c == 'L')
        {
          lambda = -1;
          f >> lambda;
          if (! f.good() || lambda<=0)
            {
              cerr << "ERROR (reading model): "
                   << "Invalid lambda: " << lambda << endl;
              exit(10);
            }
        }
      else
        {
          cerr << "ERROR (reading model): "
               << "Unrecognized line: '" << c << "'" << endl;
          exit(10);
        }
    }
  if (! wscale)
    {
      cerr << "ERROR (reading model): "
           << "This model file does not contain weights. " << endl;
      exit(10);

    }
}


istream&
operator>> (istream &f, CrfSgd &d )
{
  d.load(f);
  return f;
}


void
CrfSgd::rescale()
{
  if (wscale != 1.0)
    {
      w.scale(wscale);
      wscale = 1;
    }
}


void
CrfSgd::save(ostream &f) const
{
  // rescale weights according to wscale
  if (wscale != 1.0)
    const_cast<CrfSgd*>(this)->rescale();
  // save stuff
  f << dict;
  f << "L" << lambda << endl;
  f << "T" << t << endl;
  f << "E" << epoch << endl;
  f << "W" << w << endl;
}


ostream&
operator<<(ostream &f, const CrfSgd &d)
{
  d.save(f);
  return f;
}


void
CrfSgd::initialize(const char *tfname, const char *dfname,
                   double c, int cutoff)
{
  t = 0;
  epoch = 0;
  int n = dict.initFromData(tfname, dfname, cutoff);
  num_training = n; 
  lambda = 1 / (c * n);
  if (verbose)
    cout << "Using c=" << c << ", i.e. lambda=" << lambda << endl;
  w.clear();
  w.resize(dict.nParams());
  wscale = 1.0;
  wnorm = 0;
}

void CrfSgd::initialize_vargp(string dataFolder, ints_t indexes, double c, int cutoff){
  t = 0;
  epoch = 0;
  int n = indexes.size();
  dict.initFromFolder(dataFolder, indexes, cutoff);
  num_training = n;
  lambda = 1 / (c * n);

  if (verbose)
    cout << "Using c=" << c << ", i.e. lambda=" << lambda << endl;
  w.clear();
  w.resize(dict.nParams());
  wscale = 1.0;
  wnorm = 0;
}


double
CrfSgd::findObjBySampling(const dataset_t &data, const ivec_t &sample)
{
  double loss = 0;
  int n = sample.size();
  for (int i=0; i<n; i++)
    {
      int j = sample[i];
      Scorer scorer(data[j], dict, w, wscale);
      loss += scorer.scoreForward() - scorer.scoreCorrect();
    }
  return loss / n + 0.5 * wnorm * lambda;
}


double
CrfSgd::tryEtaBySampling(const dataset_t &data, const ivec_t &sample,
                         double eta)
{
  FVector savedW = w;
  double savedWScale = wscale;
  double savedWNorm = wnorm;
  int i, n = sample.size();
  for (i=0; i<n; i++)
    {
      int j = sample[i];
      TScorer scorer(data[j], dict, w, wscale, eta);
      scorer.gradCorrect(+1);
      scorer.gradForward(-1);
      wscale *= (1 - eta * lambda);
    }
  wnorm = dot(w,w) * wscale * wscale;
  double obj = findObjBySampling(data, sample);
  w = savedW;
  wscale = savedWScale;
  wnorm = savedWNorm;
  return obj;
}


void
CrfSgd::adjustEta(double eta)
{
  t = 1 / (eta * lambda);
  if (verbose)
    cout << " taking eta=" << eta << "  t0=" << t;
}


double CrfSgd::adjustEta(const dataset_t &data, int samples,
                  double seta, Timer *tm)
{
  ivec_t sample;
  if (verbose)
    cout << "[Calibrating] --  " << samples << " samples" << endl;
  assert(samples > 0);
  assert(dict.nOutputs() > 0);
  // choose sample
  int datasize = data.size();
  if (samples < datasize)
    for (int i=0; i<samples; i++)
      sample.push_back((int)((double)rand()*datasize/RAND_MAX));
  else
    for (int i=0; i<datasize; i++)
      sample.push_back(i);
  // initial obj
  double sobj = findObjBySampling(data, sample);
  cout << " initial objective=" << sobj << endl;
  // empirically find eta that works best
  double besteta = 1;
  double bestobj = sobj;
  double eta = seta;
  int totest = 10;
  double factor = 2;
  bool phase2 = false;
  while (totest > 0 || !phase2)
    {
      double obj = tryEtaBySampling(data, sample, eta);
      bool okay = (obj < sobj);
      if (verbose)
        {
          cout << " trying eta=" << eta << "  obj=" << obj;
          if (okay)
            cout << " (possible)" << endl;
          else
            cout << " (too large)" << endl;
        }
      if (okay)
        {
          totest -= 1;
          if (obj < bestobj) {
            bestobj = obj;
            besteta = eta;
          }
        }
      if (! phase2)
        {
          if (okay)
            eta = eta * factor;
          else {
            phase2 = true;
            eta = seta;
          }
        }
      if (phase2)
        eta = eta / factor;
    }
  // take it on the safe side (implicit regularization)
  besteta /= factor;
  // set initial t
  adjustEta(besteta);
  // message
  if  (tm && verbose)
    cout << " time=" << tm->elapsed() << "s.";
  if (verbose)
    cout << endl;

  return besteta; 
}

void
CrfSgd::crossValidate(const dataset_t &data, int num_folds, int min_exp, int max_exp, int epochs, double eta, Timer *tm){ 
    cout << "Crossvalidating to find value of C" << endl; 

    int fold_size = (int) data.size()/num_folds; 
    cout << "Using " << num_folds << " folds of size " << fold_size <<endl;
    
    int best_exp = 0; 
    double best_lambda = 0; 
    double best_err = 200;

    for (int exp_value = min_exp; exp_value < max_exp + 1; exp_value++) {
        double new_c = pow(10, exp_value); 
        lambda = 1 / (new_c * num_training);

        
        

        cout << "Testing c = " << new_c <<", lambda = " << lambda << endl;
        double avg_err = 0;
        for (int i = 0; i < num_folds; i++){ 
            
            
            //t = 1 / (eta * lambda);

            dataset_t::const_iterator test_first = data.begin() + i*fold_size; 
            dataset_t::const_iterator test_last = data.begin() + (i+1) * fold_size; 
                        
            dataset_t cv_test = dataset_t(test_first, test_last);

            dataset_t cv_train = dataset_t(data.begin(), test_first); 
            cv_train.insert(cv_train.end(), test_last, data.end()); 

            if (eta > 0)
                adjustEta(eta);
            else
                adjustEta(cv_train, 1000, 0.1, tm);


            train(cv_train, epochs, tm);
            avg_err += test(cv_test, 0); 


            w.clear();
            w.resize(dict.nParams());
            wscale = 1.0;
            wnorm = 0;
            epoch = 0;
            gen.seed(12000);
            srand(12000);
            

        }
        avg_err = avg_err/num_folds;
        if (avg_err < best_err) { 
            best_exp = exp_value; 
            best_lambda = lambda;
            best_err = avg_err; 
        }
        
        cout << "Average error: " << avg_err << ", best error: " << best_err << " (c = 1e"<<best_exp<<", lambda = "<<best_lambda<<")" << endl;

    }

    cout << "CROSSVALIDATION FINISHED. USING C = 1e" << best_exp <<", lambda = " << best_lambda << endl;
    lambda = best_lambda; 
    t = 1 / (eta * lambda);

}


void
CrfSgd::train(const dataset_t &data, int epochs, Timer *tm)
{
   

  if (t <= 0)
    {
      cerr << "ERROR (train): "
           << "Must call adjustEta() before train()." << endl;
      exit(10);
    }
      
  ivec_t shuffle;
  for (unsigned int i=0; i<data.size(); i++)
    shuffle.push_back(i);
  for (int j=0; j<epochs; j++)
    {
      
      epoch += 1;
      // shuffle examples
            
      
      std::shuffle(shuffle.begin(), shuffle.end(), gen);

            
      if (verbose)
        cout << "[Epoch " << epoch << "] --";

      if (verbose)
        cout.flush();
      // perform epoch
      for (unsigned int i=0; i<data.size(); i++)
        {
          double eta = 1/(lambda*t);
          TScorer scorer(data[shuffle[i]], dict, w, wscale, eta);

          /*if (epoch == 5 && i==8935) {
            cout << "i = " << i << endl;
            cout.flush();
          }*/


          // train

          scorer.gradCorrect(+1);
          scorer.gradForward(-1);
          // weight decay
          wscale *= (1 - eta * lambda);
          // iteration done
          t += 1;
        }
      // epoch done
      if (wscale < 1e-5)
        rescale();
      wnorm = dot(w,w) * wscale * wscale;
      cout << " wnorm=" << wnorm;
      if (tm && verbose)
        cout << " time=" << tm->elapsed() << "s.";
      if (verbose)
        cout << endl;
    }
  // this never hurts
  rescale();
}


double CrfSgd::test(const dataset_t &data, const char *conlleval, Timer *tm)
{
   if (dict.nOutputs() <= 0)
    {
      cerr << "ERROR (test): "
           << "Must call load() or initialize() before test()." << endl;
      exit(10);
    }
   opstream f;
   if (conlleval && verbose && 0)
     f.open(conlleval);
   if (verbose)
     cout << " sentences=" << data.size();
   double obj = 0;
   int errors = 0;
   int total = 0;
   double nell = 0;
   for (unsigned int i=0; i<data.size(); i++)
     {
       Scorer scorer(data[i], dict, w, wscale);
       obj += scorer.scoreForward() - scorer.scoreCorrect();
       if (0 && conlleval && verbose)
         errors += scorer.test(f, nell);
       else
         errors += scorer.test(nell);
       total += data[i].size();
     }
   obj = obj / data.size();
   if (verbose)
     cout << " loss=" << obj;
   obj += 0.5 * wnorm * lambda;
   double misrate = (double)(errors*100)/(total ? total : 1);
   if (verbose) {
     cout << " obj=" << obj
          << " err=" << errors << " (" << misrate << "%)";
     if (use_fb)
          cout << " nell=" << nell;
          }
   if (tm && verbose)
     cout << " time=" << tm->elapsed() << "s";
   if (verbose)
     cout << endl;

   return misrate;
}



void
CrfSgd::tag(const dataset_t &data)
{
   if (dict.nOutputs() <= 0)
    {
      cerr << "ERROR (test): "
           << "Must call load() or initialize() before tag()." << endl;
      exit(10);
    }
   for (unsigned int i=0; i<data.size(); i++)
     {
       double nell = 0;
       Scorer scorer(data[i], dict, w, wscale);
       scorer.test(cout, nell);
     }
}





// ============================================================
// Main


string modelFile;
string templateFile;
string trainFile;
string testFile;

string indexFile;
string dataFolder;
int ntraining = 0;

const char *conlleval = "./conlleval -q";

double c = 1;
double eta = 0;
int cutoff = 3;
int epochs = 50;
int cepochs = 5;
bool tag = false;

dataset_t train;
dataset_t test;

int min_exp = 0; 
int max_exp = 0; 
int num_folds = 0;


void
usage()
{
  cerr
    << "Usage (training): "
    << "crfsgd [options] model template traindata [devdata]" << endl
    << "Usage (tagging):  "
    << "crfsgd -t model testdata" << endl
    << "Options for training:" << endl
    << " -c <num> : capacity control parameter (1.0)" << endl
    << " -f <num> : threshold on the occurences of each feature (3)" << endl
    << " -r <num> : total number of epochs (50)" << endl
    << " -h <num> : epochs between each testing phase (5)" << endl
    << " -e <cmd> : performance evaluation command (conlleval -q)" << endl
    << " -s <num> : initial learning rate" << endl
    << " -v <index file> <training data folder> <num training> : vargpstruct-style training data" << endl
    << " -z <folder> : save the data (in vargp-compatible format format) in the given foler" << endl
    << " -b : forward-backwards (default Viterbi instead)"<<endl
    << " -x <min_exp> <max_exp> <num_folds> : cross-validation for choosing control parameter over <num_folds> folds, testing all powers of ten from 1e<min_exp> until 1e<max_exp> (included)" 
    << " -q       : silent mode" << endl;
  exit(10);
}

void
parseCmdLine(int argc, char **argv)
{
  for (int i=1; i<argc; i++)
    {
      const char *s = argv[i];
      //cout << "Read option " << s << endl;
      if (s[0]=='-')
        {
          while (s[0] == '-')
            s++;
          if (tag || s[1])
            usage();
          if (s[0] == 't')
            {
              if (i == 1)
                tag = true;
              else
                usage();
            }
          else if (s[0] == 'q')
            verbose = false;
          else if (s[0]=='b') {
            cout << "Using Forward-backward (instead of Viterbi)" << endl;
            use_fb = true;
          }
          else if (++i >= argc)
            usage();
          else if (s[0 ] == 'c')
            {
              c = atof(argv[i]);
              if (c <= 0)
                {
                  cerr << "ERROR: "
                       << "Illegal C value: " << c << endl;
                  exit(10);
                }
            }
         else if (s[0 ] == 's')
            {
              eta = atof(argv[i]);
              if (eta <= 0)
                {
                  cerr << "ERROR: "
                       << "Illegal initial learning rate: " << s << endl;
                  exit(10);
                }
            }
          else if (s[0] == 'f')
            {
              cutoff = atoi(argv[i]);
              if (cutoff <= 0 || cutoff > 1000)
                {
                  cerr << "ERROR: "
                       << "Illegal cutoff value: " << cutoff << endl;
                  exit(10);
                }
            }
          else if (s[0] == 'r')
            {
              epochs = atoi(argv[i]);
              if (epochs <= 0)
                {
                   cerr << "ERROR: "
                        << "Illegal number of epochs: " << epochs << endl;
                   exit(10);
                }
            }
          else if (s[0] == 'h')
            {
              cepochs = atoi(argv[i]);
              if (cepochs <= 0)
                {
                   cerr << "ERROR: "
                        << "Illegal number of epochs: " << cepochs << endl;
                   exit(10);
                }
            }
          else if (s[0] == 'e')
            {
              conlleval = argv[i];
              if (!conlleval[0])
                conlleval = 0;
            }
          else if (s[0] == 'v'){
            cout << "Using vargpstruct-style input data." << endl;
            vargpdata = true;
            indexFile = argv[i];
            cout << "Index file: " << indexFile << "." << endl;
            dataFolder = argv[++i];
            cout << "Using data in folder " << dataFolder << "." << endl;
            ntraining = atoi(argv[++i]);
            if (ntraining <= 0)
                {
                   cerr << "ERROR: "
                        << "Illegal number of training inputs" << ntraining << endl;
                   exit(10);
                }
            cout << "Using " << ntraining << " sentences for training." << endl;
          }
          else if (s[0] == 'x') { 
              num_folds = atoi(argv[i]); 
              min_exp = atoi(argv[++i]); 
              max_exp = atoi(argv[++i]); 
              cout << "Choosing C value by cross-validation over " << num_folds <<  " folds, testing values from 1e"<<min_exp <<" until 1e"<< max_exp << endl;
          }
          else if (s[0] == 'z') {
            save_folder = argv[i]; 
            cout << "saving data in vargp format in folder " << save_folder << endl; 
          }


          else
            {
                  cerr << "ERROR: "
                       << "Unrecognized option: " << argv[i-1] << endl;
                  exit(10);
            }
        }
      else if (tag)
        {
          if (modelFile.empty())
            modelFile = argv[i];
          else if (testFile.empty())
            testFile = argv[i];
          else
            usage();
        }
      else
        {
          if (modelFile.empty()) {
            modelFile = argv[i];
            cout << "Model file: " << modelFile << endl;
            }
          else if (templateFile.empty() && !vargpdata) {
            templateFile = argv[i];
            cout << "Template file: " << templateFile << endl;
            }
          else if (trainFile.empty() && !vargpdata) {
            trainFile = argv[i];
            cout << "Train file: " << trainFile << endl;
            }
          else if (testFile.empty() && !vargpdata) {
            testFile = argv[i];
            cout << "Test file: " << testFile << endl;
            }
          else {
            usage();
            }
        }
    }
  if (tag)
    {
      verbose = false;
      if (modelFile.empty() ||
          testFile.empty())
        usage();
    }
  else
    {
      if (!vargpdata && (modelFile.empty() ||
          templateFile.empty() ||
          trainFile.empty())) {
          usage();
        }
    }
}


int
main(int argc, char **argv)
{
  // parse args
  parseCmdLine(argc, argv);
  // initialize crf
  CrfSgd crf;

 
  if (tag)
    {
      igzstream f(modelFile.c_str());
      f >> crf;
      loadSentences(testFile.c_str(), crf.getDict(), test, 0);
      // tagging
      crf.tag(test);
    }
  else
    {
      if (vargpdata) {
          ints_t indexes = loadIndexes(indexFile.c_str());
          for (unsigned int i = 0; i < indexes.size(); i++)
            cout << indexes[i] << " ";

          crf.initialize_vargp(dataFolder, indexes, c, cutoff);
          loadSentences_vargp(dataFolder, crf.getDict(), indexes, ntraining, train, test);
          cout << "DONE!" <<endl;
      }
      else {
        crf.initialize(templateFile.c_str(), trainFile.c_str(), c, cutoff);
        loadSentences(trainFile.c_str(), crf.getDict(), train, 0);

        cout << "Training sentences: " << train.size() << endl;
        if (! testFile.empty())
          loadSentences(testFile.c_str(), crf.getDict(), test, train.size());
      }
      cout << "Test sentences: " << test.size() << endl;

      // training
      Timer tm;
      tm.start();

     

     
       if (min_exp < max_exp) {
        // Cross-validation
        crf.crossValidate(train, num_folds, min_exp, max_exp, epochs, eta, &tm);
      }
      if (eta > 0)
        crf.adjustEta(eta);
      else
        eta = crf.adjustEta(train, 1000, 0.1, &tm);



            tm.stop();
      while (crf.getEpoch() < epochs)
        {
          tm.start();
          int ce = cepochs; // (crf.getEpoch() < cepochs) ? 1 : cepochs;
          crf.train(train, ce, &tm);
          tm.stop();
          if (verbose)
            {
              cout << "Training perf:";
              crf.test(train, conlleval);
            }
          if (verbose && test.size())
            {
              cout << "Testing perf:";
              crf.test(test, conlleval);
            }
        }
      if (verbose)
        cout << "Saving model file " << modelFile << "." << endl;
      ogzstream f(modelFile.c_str());
      f << crf;
      if (verbose)
        cout << "Done!  " << tm.elapsed() << " seconds." << endl;
    }
  return 0;
}



/* -------------------------------------------------------------
   Local Variables:
   c++-font-lock-extra-types: ("\\sw+_t" "[A-Z]\\sw*[a-z]\\sw*")
   End:
   ------------------------------------------------------------- */
